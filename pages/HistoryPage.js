import React from 'react';

import { Image,Button, StyleSheet, Text, View, FlatList, Alert, Styles, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';
import Swipeout from 'react-native-swipeout';
//import HistoryComponent from '../component/HistoryComponent';
import JobAd from '../component/JobAd'; 


class HistoryPage extends React.Component {

    state = { quizz: null, refreshing: false  };
  
    refresh(){
        AsyncStorage.getItem('interviews').then(data => {
            this.setState({quizz : JSON.parse(data)})   

    })
    }

    componentDidMount(){
      this.refresh()
    }

    renderSeparator = () => { 
        return (
            <View style={{ height: 1, width: "100%", backgroundColor: "#000" }} />

        );
    }

  

    deleteQuizz () {
        AsyncStorage.removeItem('interviews');
        this.refresh();
    }

    deleteQuizzAlert = (item) => {  
        Alert.alert(
        'Confirmation',
        "Êtes-vous sûr de vouloir supprimer l'historique",[
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () => this.deleteQuizz ()},
          ]);  
    }

    render () {
        return (
            this.state.quizz != null ? (
            <View style={styles.container}>                
            <NavigationEvents onDidFocus={()=> this.refresh()}/>
            <FlatList ItemSeparatorComponent={this.renderSeparator} data = {this.state.quizz} renderItem={(element) =>  (    
                    <View style={styles.flatview} >
                        <View>
                            <Image style = {{width : 80, height : 60}} source = {{uri : element.item.sprite}}/>
                        </View>
                        <View>
                            <Text style={styles.name}>{element.item.name}</Text>
                            {element.item.success ? (
                                <Text style={styles.success}>Succès</Text>
                            ) :
                            (
                                <Text style={styles.echec}>Echec</Text>

                            )}
                        </View>
                        <View>
                            <Text style={styles.date}> {element.item.date}</Text>
                            <Text style={styles.hour}>{element.item.hour}</Text>
                        </View>
                    </View>
             ) }
                />
            <Button title="Clear History" onPress = {() => { this.deleteQuizzAlert() }}/>
                  </View>
            ):
            (                
                <View>
                    <NavigationEvents onDidFocus={()=> this.refresh()}/>
                </View>
            )
            
            
            );
            }
        }
    


    const styles = StyleSheet.create({
            container: {
                flex: 1,
                backgroundColor: '#F5FCFF',
            },
            h2text: {
              marginTop: 10,
              fontSize: 36,
              fontWeight: 'bold',
            },
            flatview: {
                flex: 2,
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingTop: 10,
                borderRadius: 2,
            },
            success: {
              color: 'green',
              fontSize: 23
            },
            echec: {
                color: 'red',
                fontSize: 23
              },


        });
export default HistoryPage;