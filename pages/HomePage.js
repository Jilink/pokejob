import React from 'react';
import { Image, StyleSheet, Text, View, AsyncStorage, TouchableHighlight, Dimensions, ImageBackground} from 'react-native';
import Loading from '../component/Loading'
import { NavigationEvents } from 'react-navigation';

import Categories from '../component/Categories'
import Salary from '../component/Salary';








class HomePage extends React.Component {

  state = {

  };

  refresh(){

  }

   // attendre que les données soient chargées :
  componentDidMount(){
    this.refresh()
  }
  
  render() {
      // alert(this.state.wea)

    return (
        1===1 ? ( // est-ce que le render est chargé ? 
        <View style={{ flex: 1, backgroundColor: "#ffc91a"}}>
          <View style={{ flex: 2, justifyContent: "space-around", flexDirection: "row", alignItems:'center'}}>
            <Salary/>
          </View>
          <View style={{ flex: 7, justifyContent: "center", marginBottom:40}}>


            <View style={{ flex: 2, justifyContent: "space-around", alignItems: "center", flexDirection: "row"}}>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Fire.png')} category={10} customStyles={{backgroundColor: "black"}}>
                <Text>Fire</Text>
              </Categories>
            </View>

            <View style={{ flex: 2, justifyContent: "space-around", alignItems: "center", flexDirection: "row"}}>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Fighting.png')} category={2} customStyles={{backgroundColor: "grey"}}>
                <Text>Fighting</Text>
              </Categories>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Fly.png')}  category={3} customStyles={{backgroundColor: "#31B0D3"}}>
                <Text>Flying</Text>
              </Categories>
            </View>

            <View style={{ flex: 2, justifyContent: "space-around", alignItems: "center", flexDirection: "row"}}>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Poison.png')} category={4} customStyles={{backgroundColor: "purple"}}>
                <Text>Poison</Text>
              </Categories>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Electric.png')}  category={13} customStyles={{backgroundColor: "brown"}}>
                <Text>ELECTRIC</Text>
              </Categories>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Rock.png')}  category={6} customStyles={{backgroundColor: "#743A04"}}>
                <Text>Rock</Text>
              </Categories>
            </View>

            
            <View style={{ flex: 2, justifyContent: "space-around", alignItems: "center", flexDirection: "row"}}>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Bug.png')} category={7} customStyles={{backgroundColor: "#7B6045"}}>
                <Text>Bug</Text>
              </Categories>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Ghost.png')}  category={8} customStyles={{backgroundColor: "beige"}}>
                <Text>Ghost</Text>
              </Categories>
            </View>

            <View style={{ flex: 2, justifyContent: "space-around", alignItems: "center", flexDirection: "row"}}>
              <Categories  navigation={this.props.navigation} source={require('../assets/CategoriesPicture/Water.png')} category={11} customStyles={{backgroundColor: "lightblue"}}>
                <Text>Water</Text>
              </Categories>

            </View>
          </View>
          {/* <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
              <NavigationEvents onDidFocus={()=> this.refresh()}/>
                
          </View> */}
        </View>
        )
        :
        (
          <Loading color = "red">
              <Text>Connexion en cours</Text>
          </Loading>
          
        )
        

    );
  }
}


const styles = StyleSheet.create({
  textinput: {
    marginLeft: 5,
    marginRight: 5,
    height: 50,
    borderColor: '#000000',
    borderWidth: 1,
    paddingLeft: 5
  }
})

export default HomePage