import React from 'react';

import { Image, StyleSheet, Text, View, AsyncStorage, FlatList} from 'react-native';
import Loading from '../component/Loading'
import { NavigationEvents } from 'react-navigation';
import { connect } from 'react-redux'
import JobAd from '../component/JobAd'

class FindJobPage extends React.Component {

   state = {
      pokemons : null

   };

   

   refresh(){
      this.setState({pokemons: null})
      this.props.pokeservice.getType(this.props.navigation.getParam('category',1))
      .then((data) => {
         this.setState({pokemons : this.props.pokeservice.getRandomPokemons(data.data.pokemon)})
      })
   }

   // attendre que les données soient chargées :
   componentDidMount(){
      this.refresh()
   }

   render() {
      // alert(this.state.wea)

      return (
         this.state.pokemons != null ? ( // est-ce que le render est chargé ? 
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                  <NavigationEvents onDidFocus={()=> this.refresh()}/>
                  <FlatList data = {this.state.pokemons} renderItem = {(element)=>  (
                     <View>
                        <JobAd navigation={this.props.navigation} category = {this.props.navigation.getParam('category',1)} element = {element}></JobAd>
                     </View>
                  )} />
            </View>
         )
         :
         (
            <Loading color = "red">
               <Text>Connexion en cours</Text>
            </Loading>
            
         )

      );
   }
}

const mapStateToProps = (stateStore) => {
   return {
      pokeservice: stateStore.servicesReducer.PokeService
   }
}

export default connect(mapStateToProps)(FindJobPage)
