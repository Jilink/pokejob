//quizzJob page
import React from 'react';
import { ImageBackground, StyleSheet, Text, View, AsyncStorage, FlatList} from 'react-native';
import Loading from '../component/Loading'
import { NavigationEvents } from 'react-navigation';
import { ActivityIndicator } from 'react-native-paper';
import QuizzService from '../services/QuizzService';

const imgCorrect = require('../assets/success.png')
const imgFalse = require('../assets/error.png')


class QuizzJob extends React.Component{
constructor (props){
super(props);
this.state={
    Loading: true,
    count: 0,
    data: null,
    notes : 0,
    imageCorrect : null,
    imageFalse : null,
    currentAnswers: null
};
}

refresh(){
    this.setState({
        Loading: true,
        count: 0,
        data: null,
        notes : 0,
        currentAnswers: null
    });
    let serv = new QuizzService()
    serv.get(this.props.navigation.getParam('jobData',3).quizz.nbQuestion,this.props.navigation.getParam('jobData',12 ).quizz.category)
    .then((data)=>{
        this.setState({
            Loading: false,
            data: data,
        })
        this.saveQuestion()
        console.log(data)

    })
}
//on récupère toutes les questions et leurs réponses depuis l'API
saveQuestion(i=0) {
    let answers = [];
    for (let answer of this.state.data.data.results[i].incorrect_answers) {
        answers.push(answer)
    }
    answers.splice(Math.random() * (answers.length), 0, this.state.data.data.results[i].correct_answer );
    this.setState({
        Loading: false,
        currentAnswers : answers
    })   
}
// on construit notre composants qui stocke les questions 
componentDidMount () {
    // this.refresh()
}
//function qui teste qu'une réponse est bonne ou mauvaise 
onPress(item){
    if(item === this.state.data.data.results[this.state.count].correct_answer){
        alert('Bonne réponse');
        this.setState({
            notes : this.state.notes + 1
        })
    }

    else{
        alert("Mauvaise réponse")
        
    }

    if (this.state.data.data.results.length == this.state.count+1) {
        alert('votre score est de: ' +this.state.notes + '/' + this.state.data.data.results.length)

        if (this.state.notes> (this.state.data.data.results.length/2)){
            console.log("bravo")
            AsyncStorage.setItem('salary', this.props.navigation.getParam('jobData',0).quizz.salary.toString());
        }
        AsyncStorage.getItem('interviews').then(data => {
            let tab = [];
            console.log(data)
            if (data !== null) {
                tab = JSON.parse(data);
            }
            let getDate = new Date()
            let date = getDate.getDate() + '/' + getDate.getMonth() +'/' + getDate.getFullYear()
            let hour = getDate.getHours() +':' + getDate.getMinutes() +':' + getDate.getSeconds()
            let pokemon = {
                name: this.props.navigation.getParam('jobData',null).pokemon.name,
                sprite: this.props.navigation.getParam('jobData',null).pokemon.sprite,
                date: date,
                hour: hour,
                success: this.state.notes> (this.state.data.data.results.length/2)
            }
            tab.push(pokemon);
            AsyncStorage.setItem('interviews', JSON.stringify(tab))
                .then(() => {
                    this.props.navigation.navigate('History')
                })
                .catch((err) => {
                    // alert(err);
                });
        });
    }
    else {
        this.setState({
            count: this.state.count + 1  
        })
        this.saveQuestion(this.state.count +1)
    }

}
    
render(){
    if(this.state.data == null || this.state.currentAnswers == null){
        return(
            
            <View style={styles.container}>
                <NavigationEvents onDidFocus={()=> this.refresh()}/>
                <ActivityIndicator />
            </View>
        )  
    }
    else{    
                
    return(
        <View style={styles.container}>
            <NavigationEvents onDidFocus={()=> this.refresh()}/>
            <ImageBackground 
             style={styles.bgImage} 
             source = {{uri:"https://i.pinimg.com/originals/cb/33/49/cb3349b86ca661ca61ae9a36d88d70d4.png"} }
             resizeMode = "cover"
            >
            <View style={styles.headerContainer}>
                <Text style = {styles.categoryText}>{unescape(this.state.data.data.results[0].category)}</Text>
            </View>
            <View style={styles.textQuestion}>
                <Text style = {styles.categoryText}>Question {this.state.count + 1}</Text>
            </View>
            <View style={styles.headerContainer}>
                <Text 
                    style={styles.titleText}>
                     {unescape(this.state.data.data.results[this.state.count].question)}
                </Text> 
            </View>
            <View style = {styles.content}>
            
            <FlatList data = {this.state.currentAnswers} renderItem = {({ item }) =>(
                <Text style={styles.items}  onPress = {()=> {this.onPress(item)}}>{unescape(item)}</Text>
            
                )}/>
            </View> 
            </ImageBackground>
        </View> 
    );
    
    }
 }  
    
}

// styles 
const styles = StyleSheet.create({
   
    container:{
        flex: 1,
        backgroundColor: '#fff',
    },
   content:{
        flex: 4,
        alignContent: 'center',
        textAlign: 'center',
        flexDirection: 'row',
        marginTop: 20

   },
    items:{
        flex: 1,
        textAlign: 'center',
        width: 350,
        height: 70,
        margin: 5,     
        borderRadius: 10,
        marginLeft: 10,
        marginRight: 10,
        shadowOffset:{  width: 10,  height: 10,  },
        fontSize: 20,
        
        color: 'white',
        backgroundColor: 'rgba(52, 52, 52, 0.6)'
    
    },
    titleText:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 24,
        paddingLeft: 24,
        paddingTop: 12,
        paddingBottom: 12,
        backgroundColor : '#00BCD4',
        borderWidth: 2,
        borderRadius: 8,
        borderColor: '#ffffff',
        margin : 8,
        fontSize: 27,
        marginTop : 10,
    },
    TextReponse:{
        flex: 1,
        alignContent: 'center',
        alignItems: 'center'
    },
    textCorrect:{
        alignSelf:'center',
        width: 275,
        height: 75,
        backgroundColor:'green'
    },
    textError:{
        alignSelf:'center',
        width: 275,
        height: 75,
        backgroundColor:'red'
    },
    bgImage :{
        flex: 1,
        height: '100%',
        width: '100%',
        backgroundColor: 'transparent',

        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    categoryText:{
        fontWeight: '300',
        color: '#DF013A',
        textAlign:'center',
        fontSize: 28,
        fontWeight: '900'
    },
    textQuestion:{
        fontWeight: '300',
        color: '#DF013A',
        marginTop: 25,
        textAlign:'center',
        fontSize: 28,
        fontWeight: '900',
        textDecorationLine:'underline'
    },
    success:{
        width: 50,
        height: 50
    },
    error:{
        width: 50,
        height: 50
    }
})

export default QuizzJob