import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import { Image,StyleSheet } from 'react-native';
import QuizzJob from '../pages/QuizzJob'
import ProfilePage from '../pages/ProfilePage'
import { createStackNavigator } from 'react-navigation-stack';
import HomePage from '../pages/HomePage';
import HistoryPage from '../pages/HistoryPage';
import FindJobPage from '../pages/FindJobPage';
import Icon from 'react-native-vector-icons/Ionicons';
import React from 'react';

const tabNavigator = createMaterialBottomTabNavigator(
  {
    FindJob: {
        screen: FindJobPage,
        navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon color={tintColor} size={25} name={'ios-search'} />
      )
      }
    },
    home: {
      screen: HomePage,
      navigationOptions: {
          tabBarLabel: 'Home',
          tabBarIcon: ({tintColor}) => (
              <Icon color={tintColor} size={25} name={'ios-home'} />
          )
      }
  },
  Profile: {
      screen: ProfilePage,
      navigationOptions: {
          tabBarLabel: 'Profile',
          tabBarIcon: ({tintColor}) => (
              <Icon color={tintColor} size={25} name={'ios-camera'} />
          )
      }
  },
  QuizzJob: {
    screen: QuizzJob,
    navigationOptions: {
      tabBarLabel: 'Quizz',
      
  }
  },
  History: {
      screen: HistoryPage,
      navigationOptions: {
          tabBarLabel: 'History',
          tabBarIcon: ({tintColor}) => (
              <Icon color={tintColor} size={25} name={'ios-archive'} />
          )
      }
  }
},
{
    initialRouteName: 'home'
},
);

export default tabNavigator;
