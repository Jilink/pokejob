import { createAppContainer, createSwitchNavigator } from "react-navigation"
import MainTabNavigator from './MainTabNavigator';
import Navigator from './MainTabNavigator';

export default createAppContainer(MainTabNavigator)