import axios from "axios"
import React from 'react';

const key = 'f2fdb141df8e2c8c4573fdb7a40ca6ff'
const url = `https://pokeapi.co/api/v2/`

class PokeService{

  getType(type = 1){
    return axios.get(`${url}type/${type}`)
  }

  getPokemon(id = 1){
    return axios.get(`${url}pokemon/${id}`)
  }

  getPokemonUrl(url =`https://pokeapi.co/api/v2/pokemon/1`){
    return axios.get(url)
  }

  getRandomPokemons(pokemons, number= 20){
    let randoPokemons = []
    for (let i =0;i< number; i++){
      let randoNumber = Math.floor(Math.random() * pokemons.length)
      randoPokemons.push(pokemons[randoNumber].pokemon.url)
    }
    return randoPokemons
  }

  // getCityWeather(city= 'nanterre'){
  //   if(!city) return this.getWeatherLatLong()
  //   return axios.get(`${url}&q=${city.toLowerCase()},fr`)

  // } 
  

  // getWeatherLatLong(){
  //   return new Promise(function (resolve, reject) {
  //     navigator.geolocation.getCurrentPosition(resolve, reject)
  //   }).then((position) => {
  //     const location = JSON.stringify(position);
  //     console.log("link",`${url}&lat=${position.coords.latitude}&lon=${position.coords.longitude}`)
  //     return axios.get(`${url}&lat=${position.coords.latitude}&lon=${position.coords.longitude}`)
  //   }).catch((err) => {
  //     console.error(err.message);
  //   });

  // }

  // getWeatherHome(){
  //   return axios.get(`${url}&q=nanterre,fr`)
  // }
}


export default PokeService;
