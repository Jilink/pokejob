import axios from 'axios';

 const url = "https://opentdb.com/api.php?";

 class QuizzService{

    get(amount = 5, category = 12, difficulty = "easy"){
        return axios.get(url +`amount=${amount}&category=${category}&difficulty=${difficulty}&encode=url3986`);
    }
 }


 export default QuizzService;