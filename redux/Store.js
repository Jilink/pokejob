import {createStore, combineReducers, applyMiddleware} from "redux"
import ServicesReducer from "./reducers/ServicesReducer";
// import  from "redux-thunk"

const rootReducer = combineReducers({
  servicesReducer: ServicesReducer,
  test: ServicesReducer,
  // citiesReducer: CitiesReducer
})
export const store = createStore(rootReducer);
