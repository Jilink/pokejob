import { AsyncStorage } from 'react-native';

export const QUIZZ_INIT = 'QUIZZ_INIT';
export const QUIZZ_ADD = 'QUIZZ_ADD';

export const initAsync = () => {
    return dispatch => {
        AsyncStorage.getItem('quizz').then(data => {
            return dispatch({ type: QUIZZ_INIT, payload: JSON.parse(data) });
        });
    };
}

export const addAsync = () => {
    return dispatch => {
        AsyncStorage.getItem('quizz').then(data => {
            let tab = [];
            if (data !== null) {
                tab = JSON.parse(data);
            }
            tab.push(this.state.pokeData);
            AsyncStorage.setItem('quizz', JSON.stringify(tab))
                .then(() => {
                    return dispatch({ type: QUIZZ_INIT, payload: tab });
                });
        });
    }
}