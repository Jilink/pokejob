import { QUIZZ_INIT } from '../actions/ExampleAction';

const INITIAL_STATE = {
    quizz: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case QUIZZ_INIT:
            return { quizz: action.payload };
    }
    return state;
}