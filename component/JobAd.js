import React from 'react';
import { ActivityIndicator } from 'react-native-paper';
import PropTypes from 'prop-types';
import { Image, StyleSheet, Text, View, TouchableHighlight, AsyncStorage} from 'react-native';
import Swipeout from 'react-native-swipeout';
import Loading from '../component/Loading'
import { connect } from 'react-redux'


class JobAd extends React.Component {

  state = {
    pokeData : null,
    salary : 500,
    currentSalary : 0
  };
  LoadPokemon(){
    AsyncStorage.getItem('salary')
    .then((data)=> {
      if (data!=null ){
        this.setState ({currentSalary : data  })
        this.setState({salary : (data*2 + 500) - Math.floor(Math.random() * (data*2 + 500)/2)})
      }
      else{
        this.setState({salary : (500) - Math.floor(Math.random() * (500)/2)})

      }
    })
    this.props.pokeservice.getPokemonUrl(this.props.element.item).then((resp) => {
      return this.props.pokeservice.getPokemonUrl(resp.data.forms[0].url)
    })
    .then((resp) =>{
      this.setState({pokeData : resp.data})
    })
  }

  convertCategory(category){
    switch (category) {
      case 10: //fire to music
        return 12
      case 2: //figthing to politic
        return 24
      case 3: //flying to sport
        return 21
      case 4: //poison to maths
        return 19
      case 13: //Lighting to Video Games
        return 15
      case 6: //rock to geography
        return 22
      case 7: //bug to nature
        return 17
      case 8: //ghost to history
        return 23
      case 11: //water to art
        return 25
    
      default:
        return 12
    }
  }
  convertCategoryToString(category){
    switch (category) {
      case 10: //fire to music
        return "music"
      case 2: //figthing to politic
        return "politics"
      case 3: //flying to sport
        return "sport"
      case 4: //poison to maths
        return "maths"
      case 13: //Lighting to Video Games
        return "video games"
      case 6: //rock to geography
        return "geography"
      case 7: //bug to nature
        return "nature"
      case 8: //ghost to history
        return "history"
      case 11: //water to art
        return "art"
    
      default:
        return "music"
    }
  }

  onPress(){
    // going to the quizz that fits
    let QuizzCategory = this.convertCategory(this.props.category) 

    this.props.navigation.navigate('QuizzJob', {jobData: {
                                                  quizz : {category : QuizzCategory, nbQuestion : Math.floor(this.state.salary/100), salary: this.state.salary},
                                                  pokemon : {
                                                          name: this.state.pokeData.name,
                                                          sprite: this.state.pokeData.sprites.front_default!=null? this.state.pokeData.sprites.front_default : `https://icon-library.net/images/pokeball-icon-transparent/pokeball-icon-transparent-7.jpg`,
                                                        } 
                                                }})
    // AsyncStorage.getItem('interviews').then(data => {
    //     let tab = [];
    //     console.log(data)
    //     if (data !== null) {
    //         tab = JSON.parse(data);
    //     }
    //     let getDate = new Date()
    //     let date = getDate.getDate() + '/' + getDate.getMonth() +'/' + getDate.getFullYear()
    //     let hour = getDate.getHours() +':' + getDate.getMinutes() +':' + getDate.getSeconds()
    //     let pokemon = {
    //       name: this.state.pokeData.name,
    //       sprite: this.state.pokeData.sprites.front_default!=null? this.state.pokeData.sprites.front_default : `https://icon-library.net/images/pokeball-icon-transparent/pokeball-icon-transparent-7.jpg`,
    //       date: date,
    //       hour: hour,
    //       success: false
    //     }
    //     tab.push(pokemon);
    //     AsyncStorage.setItem('interviews', JSON.stringify(tab))
    //         .then(() => {
    //             // this.props.navigation.goBack();
    //         })
    //         .catch((err) => {
    //             // alert(err);
    //         });
    // });
  }

  componentDidMount(){
    this.LoadPokemon()
  }

  render(rowData) {
    let swipeBtns = [{
      text: 'Delete',
      backgroundColor: 'red',
      underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
      onPress: () => { this.props.onDelete(this.props.element.item.name) }
    },
    {
      text: 'Favoris',
      backgroundColor: 'blue',
      underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
      onPress: () => { this.props.newHome(this.props.element.item.name) }
    }
  ];

    return (
      this.state.pokeData != null ? (
          <TouchableHighlight underlayColor = '#ffefa2' style = {styles.jobAd} onPress = {()=> {this.onPress()}}>
            <View>
              <View style = {styles.aligned} key={this.props.element.item}>
                <View>
                  <Image style = {{width : 80, height : 80}} source = {{uri : this.state.pokeData.sprites.front_default!=null? this.state.pokeData.sprites.front_default : `https://icon-library.net/images/pokeball-icon-transparent/pokeball-icon-transparent-7.jpg`}}/>
                </View>
                <View style = {{justifyContent : "space-around"}}>
                  <Text style = {styles.titleText}>BY {(this.state.pokeData.name).toUpperCase()}</Text>
                  {/* Salaire + ou - 500 (ou nouveau salaire) le nombre de question va dépendre du salaire visé */}
                  <Text style = {[styles.SalaryText,]}>Salary : {this.state.salary}€ </Text>
                </View>
              </View>
              <Text style = {styles.normalText}>To get this job, you have to anwser {Math.floor(this.state.salary/100)} about {this.convertCategoryToString(this.props.category)} </Text>
            </View>
          </TouchableHighlight>) :(

      <Loading color = "green">
        <Text>Loading ...</Text>
      </Loading>
      )
    )
  }
}

const styles = StyleSheet.create({
  jobAd: {
    borderRadius: 10,
    borderWidth: 0.8,
    marginBottom: 2 
  },
  aligned:{
    display:'flex',
    flexDirection: 'row', 
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#ee4e31'
  },
  normalText: {
    fontSize: 16,
    padding: 10
  },
  SalaryText: {
    fontSize: 17,
    fontWeight: 'bold',
  },
})

const mapStateToProps = (stateStore) => {
  return {
    pokeservice: stateStore.servicesReducer.PokeService
  }
}

export default connect(mapStateToProps)(JobAd)