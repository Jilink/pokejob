import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Dimensions, ImageBackground } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import PropTypes from 'prop-types';
import { Asset } from 'expo-asset';




class Categories extends React.Component {
  static propTypes = {
    displayColor: PropTypes.string.isRequired // oblige à remplir la propriété 
  };
  render() {
    return (
      <TouchableHighlight
      style = {[{
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.2,
        height: Dimensions.get('window').width * 0.2,
        justifyContent: 'center',
        alignItems: 'center',
        shadowOffset:{  width: 5,  height: 5,  },
        shadowColor: 'grey',
        shadowOpacity: 0.5,
      }, this.props.customStyles]}
      underlayColor = 'lightgrey'
      // onPress = { () => console.log(this.props.category)} >
      onPress={() => this.props.navigation.navigate('FindJob', {category: this.props.category })} >
        <View>
          <ImageBackground style={{width: 50, height: 50}} source={this.props.source}/>
          {/* {this.props.children} */}
        </View>
      

    </TouchableHighlight>
    );
  }
}

export default Categories