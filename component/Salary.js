import React from 'react';
import { StyleSheet, Text, View, AsyncStorage} from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import PropTypes from 'prop-types';
import { NavigationEvents } from 'react-navigation';





class Salary extends React.Component {
  static propTypes = {
    displayColor: PropTypes.string.isRequired // oblige à remplir la propriété 
  };

  state = {
    salary: 0
  };

  refresh(){
    AsyncStorage.getItem('salary')
    .then((data)=> {
      if (data!=null ){
        this.setState ({salary : data  })
      }
    })
  }

  componentDidMount(){
    this.refresh()
  }
  
  render() {
    return (
          <View>
              <NavigationEvents onDidFocus={()=> this.refresh()}/>
              <Text>Salary : {this.state.salary} €</Text>
          </View>
    );
  }
}

export default Salary